function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;
  subClass.__proto__ = superClass;
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

var startDateFormat = "ddd, DD MMM YYYY h:mm";
var endDateFormat = "h:mmA";
var dateFormat = "ddd, DD MMM YYYY h:mm A";
var axiosInstance = axios.create();

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

var RecurrenceInfo = function RecurrenceInfo(props) {
  function onChange(e) {
    props.setRecurrence("enabled", e.target.checked);
  }

  return /*#__PURE__*/React.createElement("div", {
    key: "recurrence",
    style: {
      width: "100%"
    }
  }, /*#__PURE__*/React.createElement("div", {
    className: "form-group row"
  }, /*#__PURE__*/React.createElement("div", {
    className: "form-check col-sm-2"
  }, /*#__PURE__*/React.createElement("input", {
    className: "form-check-input",
    type: "checkbox",
    value: props.recurrence.enabled,
    onClick: onChange
  }), /*#__PURE__*/React.createElement("label", {
    className: "form-check-label",
    htmlFor: "defaultCheck1"
  }, "Repeat"))), props.recurrence.enabled && /*#__PURE__*/React.createElement("div", {
    key: "repeatDetails"
  }, /*#__PURE__*/React.createElement("div", {
    className: "form-group row"
  }, /*#__PURE__*/React.createElement("label", {
    className: "form-check-label col-sm-2"
  }, /*#__PURE__*/React.createElement("b", null, "Repeat Frequency")), /*#__PURE__*/React.createElement("div", {
    className: "col-sm-10"
  }, /*#__PURE__*/React.createElement("select", {
    value: props.recurrence.repeatFrequency,
    onChange: function onChange(e) {
      return props.setRecurrence("repeatFrequency", e.target.value);
    }
  }, /*#__PURE__*/React.createElement("option", {
    value: "daily"
  }, "Daily"), /*#__PURE__*/React.createElement("option", {
    value: "weekly"
  }, "Weekly"), /*#__PURE__*/React.createElement("option", {
    value: "monthly"
  }, "Monthly"), /*#__PURE__*/React.createElement("option", {
    value: "yearly"
  }, "Yearly"), "3"))), /*#__PURE__*/React.createElement("div", {
    className: "form-group row"
  }, /*#__PURE__*/React.createElement("label", {
    className: "form-check-label col-sm-2"
  }, /*#__PURE__*/React.createElement("b", null, "Repeat Interval:")), /*#__PURE__*/React.createElement("div", {
    className: "col-sm-10"
  }, /*#__PURE__*/React.createElement("input", {
    id: "repeatInterval",
    type: "text",
    onChange: function onChange(e) {
      return props.setRecurrence("repeatInterval", e.target.value);
    },
    value: props.recurrence.repeatInterval
  }), /*#__PURE__*/React.createElement("div", {
    style: {
      color: "red"
    }
  }, props.recurrenceErrors.repeatInterval))), /*#__PURE__*/React.createElement("div", {
    className: "form-group row"
  }, /*#__PURE__*/React.createElement("label", {
    className: "form-check-label col-sm-2"
  }, /*#__PURE__*/React.createElement("b", null, "Repeat Until:")), /*#__PURE__*/React.createElement("div", {
    className: "col-sm-10"
  }, /*#__PURE__*/React.createElement("div", {
    className: "form-check"
  }, /*#__PURE__*/React.createElement("input", {
    type: "radio",
    className: "form-check-input",
    name: "repeatUntilType",
    value: "noOfOccurrences",
    defaultChecked: props.recurrence.repeatUntilType == "noOfOccurrences" ? "enabled" : "",
    onClick: function onClick(e) {
      return props.setRecurrence("repeatUntilType", "noOfOccurrences");
    }
  }), /*#__PURE__*/React.createElement("label", null, "No of occurrences"), /*#__PURE__*/React.createElement("div", {
    style: {
      color: "red"
    }
  }, props.recurrenceErrors.noOfOccurrences)), /*#__PURE__*/React.createElement("input", {
    type: "text",
    value: props.recurrence.noOfOccurrences,
    onChange: function onChange(e) {
      return props.setRecurrence("noOfOccurrences", e.target.value);
    },
    disabled: props.recurrence.repeatUntilType == "untilDate" ? "disabled" : ""
  }), /*#__PURE__*/React.createElement("div", {
    className: "form-check"
  }, /*#__PURE__*/React.createElement("input", {
    type: "radio",
    className: "form-check-input",
    name: "repeatUntilType",
    value: "untilDate",
    onClick: function onClick(e) {
      return props.setRecurrence("repeatUntilType", "untilDate");
    }
  }), /*#__PURE__*/React.createElement("label", null, "Until Date (YYYY-MM-DD)"), /*#__PURE__*/React.createElement("div", {
    style: {
      color: "red"
    }
  }, props.recurrenceErrors.untilDate)), /*#__PURE__*/React.createElement("input", {
    type: "text",
    value: props.recurrence.untilDate,
    onChange: function onChange(e) {
      return props.setRecurrence("untilDate", e.target.value);
    },
    disabled: props.recurrence.repeatUntilType == "noOfOccurrences" ? "disabled" : ""
  })))));
};

var TermBookingView = function TermBookingView(_ref) {
  var events = _ref.events;

  var _React$useState = React.useState([]),
      subEvents = _React$useState[0],
      setSubEvents = _React$useState[1];

  var dateFormat = "ddd, DD MMM YYYY h:mmA";
  React.useEffect(function () {
    if (events) {
      var slicedEvents = events.slice(0, 3);
      setSubEvents(slicedEvents);
    }
  }, [events]);
  return /*#__PURE__*/React.createElement("div", {
    className: "form-group row"
  }, /*#__PURE__*/React.createElement("label", {
    className: "col-sm-2 col-form-label"
  }, /*#__PURE__*/React.createElement("b", null, "Session Times:")), /*#__PURE__*/React.createElement("div", {
    className: "col-sm-10"
  }, subEvents && subEvents.map(function (e) {
    return /*#__PURE__*/React.createElement("p", null, moment(new Date(e.startTime)).format(dateFormat));
  }), subEvents.length != events.length && /*#__PURE__*/React.createElement("a", {
    href: "#",
    onClick: function onClick() {
      return setSubEvents(events);
    }
  }, "Show More...")));
};

var isReady = function isReady() {
  return typeof window !== 'undefined' && typeof window.grecaptcha !== 'undefined' && typeof window.grecaptcha.render === 'function';
};

var Recaptcha = /*#__PURE__*/function (_React$Component) {
  _inheritsLoose(Recaptcha, _React$Component);

  function Recaptcha(props) {
    var _this;

    _this = _React$Component.call(this, props) || this;
    _this._renderGrecaptcha = _this._renderGrecaptcha.bind(_assertThisInitialized(_this));
    _this.reset = _this.reset.bind(_assertThisInitialized(_this));
    _this.state = {
      ready: isReady(),
      widget: null
    };
    _this.readyCheck = null;

    if (!_this.state.ready && typeof window !== 'undefined') {
      console.log("start ready check");
      _this.readyCheck = setInterval(_this._updateReadyState.bind(_assertThisInitialized(_this)), 1000);
    }

    return _this;
  }

  var _proto = Recaptcha.prototype;

  _proto.componentDidMount = function componentDidMount() {
    console.log("componentDidMount read=" + this.state.ready);

    if (this.state.ready) {
      this._renderGrecaptcha();
    }
  };

  _proto.componentWillUnmount = function componentWillUnmount() {
    clearInterval(this.readyCheck);
  };

  _proto.reset = function reset() {
    var _this$state = this.state,
        ready = _this$state.ready,
        widget = _this$state.widget;

    if (ready && widget !== null) {
      grecaptcha.reset(widget);
    }
  };

  _proto.execute = function execute() {
    var _this$state2 = this.state,
        ready = _this$state2.ready,
        widget = _this$state2.widget;

    if (ready && widget !== null) {
      grecaptcha.execute(widget);
    }
  };

  _proto._updateReadyState = function _updateReadyState() {
    console.log("updateReadyState");

    if (isReady()) {
      console.log("updateReadyState: ready");
      this.setState({
        ready: true
      });
      clearInterval(this.readyCheck);
    } else {
      console.log("updateReadyState: not ready");
    }
  };

  _proto._renderGrecaptcha = function _renderGrecaptcha() {
    this.state.widget = grecaptcha.render(this.props.elementID, {
      sitekey: this.props.sitekey,
      callback: this.props.verifyCallback ? this.props.verifyCallback : undefined,
      theme: this.props.theme,
      type: this.props.type,
      size: this.props.size,
      tabindex: this.props.tabindex,
      hl: this.props.hl,
      badge: this.props.badge
    });
  };

  _proto.render = function render() {
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      id: this.props.elementID,
      className: "g-recaptcha",
      "data-sitekey": this.props.sitekey,
      "data-theme": this.props.theme,
      "data-type": this.props.type,
      "data-size": this.props.size,
      "data-badge": this.props.badge,
      "data-tabindex": this.props.tabindex
    }));
  };

  return Recaptcha;
}(React.Component);

var BookingComponent = /*#__PURE__*/function (_React$Component2) {
  _inheritsLoose(BookingComponent, _React$Component2);

  function BookingComponent(props) {
    var _this2;

    _this2 = _React$Component2.call(this, props) || this;

    _this2.setRecurrence = function (attribute, value) {
      var _extends2;

      _this2.setState({
        recurrence: _extends({}, _this2.state.recurrence, (_extends2 = {}, _extends2[attribute] = value, _extends2))
      }, _this2.validateRecurrence);
    };

    _this2.setAssetIds = function (value) {
      console.log("assetId: " + value);

      _this2.setState({
        assetIds: value
      }, _this2.fetchAsset);
    };

    _this2.setStartDate = function (value) {
      var DATE_FORMAT = "YYYY-MM-DDTHH:mm:ssZ";
      var startTime = moment(value).format(DATE_FORMAT);

      _this2.setState({
        startDate: startTime
      }, function () {
        return _this2.fetchSlots(false);
      });
    };

    _this2.fetchSlots = function (isTerm) {
      console.log("fetchSlots");
      var data = {
        assetId: _this2.state.assetIds
      };

      if ("filterDuplicates" in _this2.props) {
        data["filterDuplicates"] = _this2.props.filterDuplicates;
      }

      if (_this2.state.startDate) {
        data["startTime"] = _this2.state.startDate;
      }

      axiosInstance.post(_this2.props.bsaUrl + "/view-bookings", data).then(function (response) {
        if (response.data.responseCode) {
          _this2.addError("Error: " + response.data.responseDescription);

          console.log(JSON.stringify(response.data.responseDescription));
        } else {
          var assets = response.data.assets;
          var events = [];

          for (var i = 0; i < assets.length; i++) {
            var asset = assets[i];

            if (asset["status"]) {
              _this2.addError("Error: " + asset["status"]);

              console.log("Error: " + asset["status"]);
            }

            var recurrences = asset["recurrences"];

            for (var j = 0; j < recurrences.length; j++) {
              var recurrenceEvents = recurrences[j]["events"];

              for (var k = 0; k < recurrenceEvents.length; k++) {
                recurrenceEvents[k].recurrenceId = recurrences[j]["recurrenceId"];
                recurrenceEvents[k].assetId = asset.assetId;
              }

              events = events.concat(recurrenceEvents);
            }
          }

          events.sort(function (a, b) {
            return a.startTime > b.startTime ? 1 : -1;
          });
          var eventMap = {};

          for (var _i = 0; _i < events.length; _i++) {
            var event = events[_i];
            eventMap[event.eventId] = event;
          }

          if (!isTerm && events.length > 0) {
            console.log("selecting first event: " + events[0].eventId);

            _this2.setState({
              events: events,
              eventMap: eventMap,
              selectedEvents: [events[0]]
            });
          } else {
            _this2.setState({
              events: events,
              eventMap: eventMap,
              selectedEvents: []
            });
          }
        }
      });
    };

    _this2.componentWillMount = function () {
      _this2.responseInterceptor = axiosInstance.interceptors.response.use(function (res) {
        return res;
      }, function (error) {
        var isAppError = error.response && error.response.data && error.response.data.responseDescription;
        var message = isAppError ? error.response.data.responseDescription : "Unexpected error";

        _this2.setState({
          errors: [].concat(_this2.state.errors, [message])
        });

        window.scrollTo(0, _this2.myRef.current.offsetTop);
        return Promise.reject(error);
      });
    };

    _this2.addError = function (error) {
      _this2.setState(function (prevState) {
        return {
          errors: [].concat(prevState.errors, [error])
        };
      });
    };

    _this2.fetchAsset = function () {
      if (!_this2.state.assetIds) {
        return null;
      }

      var baseUrl = _this2.props.assetUrl + "/asset_attribute_info";
      var attributeUrl = baseUrl + "?asset_id=eq." + _this2.state.assetIds[0];
      axiosInstance.get(attributeUrl).then(function (response) {
        var assetAttributes = response.data;
        var attributeMap = assetAttributes.reduce(function (map, obj) {
          map[obj.asset_attribute_name] = obj.asset_attribute_value;
          return map;
        }, {});
        console.log("recaptcha: " + attributeMap["recaptcha"]);
        var recaptcha = attributeMap["recaptcha"];
        var booking_user_identification = attributeMap["booking_user_identification"];
        var channel = null;

        switch (booking_user_identification) {
          case "email":
          case "email_or_phone":
            channel = "email";
            break;

          case "phone":
            channel = "mobile";
            break;
        }

        _this2.setState({
          termBooking: attributeMap["booking_type_term"],
          bookingTypeRecurring: attributeMap["booking_type_recurring"],
          sms: attributeMap["sms"],
          bookingTypeBlock: attributeMap["booking_type_block"],
          countryCode: attributeMap["country"],
          bookingUserIdentification: booking_user_identification,
          recaptcha: recaptcha,
          channel: channel
        }, function () {
          return _this2.fetchSlots(attributeMap["booking_type_term"] == "enabled");
        });
      });
    };

    _this2.componentDidMount = function () {
      _this2.fetchAsset();
    };

    _this2.validate = function () {
      var valid = true;

      if (!_this2.state.userName) {
        _this2.setState({
          userNameError: "You must enter a name"
        });

        valid = false;
      } else {
        _this2.setState({
          userNameError: ""
        });
      }

      if (_this2.state.channel == "mobile") {
        if (!_this2.state.userMobile) {
          _this2.setState({
            userMobileError: "You must enter a mobile number"
          });

          valid = false;
        } else {
          _this2.setState({
            userMobileError: ""
          });
        }
      } else if (_this2.state.channel == "email") {
        if (!_this2.state.userEmail) {
          _this2.setState({
            userEmailError: "You must enter an email address"
          });

          valid = false;
        } else if (!validateEmail(_this2.state.userEmail)) {
          _this2.setState({
            userEmailError: "You must provide a valid email address"
          });
        } else {
          _this2.setState({
            userEmailError: ""
          });
        }
      }

      if (!_this2.validateRecurrence()) {
        valid = false;
      }

      return valid;
    };

    _this2.makeBooking = function (e) {
      e.preventDefault();

      if (!_this2.validate()) {
        return;
      }

      var data = {
        "firstName": _this2.state.userName
      };
      var channel = _this2.state.channel;

      if (channel == "email") {
        data["email"] = _this2.state.userEmail;
      } else if (channel == "mobile") {
        data["mobile"] = _this2.state.userMobile;
      }

      if (_this2.state.recaptcha && _this2.state.recaptcha != "none") {
        var recaptchaResponse = grecaptcha.getResponse();

        if (recaptchaResponse) {
          data["recaptchaToken"] = recaptchaResponse;
        }
      }

      if (_this2.state.termBooking == "enabled") {
        console.log("ASSETID=" + _this2.state.assetIds[0]);
        data["assetId"] = _this2.state.assetIds[0];
      } else if (_this2.state.selectedEvents && _this2.state.selectedEvents.length > 0) {
        data["events"] = _this2.state.selectedEvents.map(function (e) {
          return _this2.state.eventMap[e.eventId];
        });
        data["assetId"] = data["events"][0].assetId;
      }

      var recurrence = _this2.state.recurrence;

      if (recurrence.enabled) {
        data["repeatInterval"] = recurrence.repeatInterval;
        data["repeatFrequency"] = recurrence.repeatFrequency;

        if (recurrence.repeatUntilType == "noOfOccurrences") {
          data["noOfOccurrences"] = recurrence.noOfOccurrences;
        } else {
          data["untilDate"] = recurrence.untilDate;
        }
      }

      var makeBookingUrl = _this2.props.bsaUrl + "/make-booking";
      console.log(JSON.stringify(data));
      axiosInstance.post(makeBookingUrl, data).then(function (response) {
        _this2.props.makeBookingHandler(response.data);
      });
    };

    _this2.state = {
      assetIds: _this2.props.assetIds,
      startDate: null,
      events: [],
      eventMap: {},
      selectedEvents: [],
      userName: "",
      userEmail: "",
      userMobile: "",
      recurrence: {
        enabled: false,
        repeatFrequency: "daily",
        repeatInterval: 1,
        noOfOccurrences: 1,
        untilDate: "2030-01-01",
        repeatUntilType: "noOfOccurrences"
      },
      recurrenceErrors: {
        repeatInterval: "",
        noOfOccurrences: "",
        untilDate: ""
      },
      userNameError: "",
      userMobileError: "",
      errors: [],
      sms: false
    };
    _this2.myRef = React.createRef();
    return _this2;
  }

  var _proto2 = BookingComponent.prototype;

  _proto2.validateRecurrence = function validateRecurrence() {
    var valid = true;
    var repeatInterval = this.state.recurrence.repeatInterval;

    var newRecurrenceErrors = _extends({}, this.state.recurrenceErrors);

    console.log("repeatInterval: " + repeatInterval);

    if (!repeatInterval) {
      newRecurrenceErrors.repeatInterval = "You must provide a repeat interval";
      valid = false;
    } else if (isNaN(repeatInterval)) {
      newRecurrenceErrors.repeatInterval = "Repeat interval must be numeric";
      valid = false;
    } else {
      newRecurrenceErrors.repeatInterval = "";
    }

    if (this.state.recurrence.repeatUntilType == "noOfOccurrences") {
      var noOfOccurrences = this.state.recurrence.noOfOccurrences;
      console.log("noOfOccurrences: " + noOfOccurrences);

      if (!noOfOccurrences) {
        newRecurrenceErrors.noOfOccurrences = "You must provide number of occurrences";
        valid = false;
      } else if (isNaN(noOfOccurrences)) {
        newRecurrenceErrors.noOfOccurrences = "Number of occurrences must be numeric";
        valid = false;
      } else {
        newRecurrenceErrors.noOfOccurrences = "";
      }
    } else if (this.state.recurrence.repeatUntilType == "untilDate") {
      var untilDate = this.state.recurrence.untilDate;
      console.log("untilDate: " + untilDate);

      if (!untilDate) {
        newRecurrenceErrors.untilDate = "You must provide an until date";
        valid = false;
      } else if (!moment(untilDate, "YYYY-MM-DD").isValid()) {
        newRecurrenceErrors.untilDate = "Until date must be provided in YYYY-MM-DD format";
        valid = false;
      } else {
        newRecurrenceErrors.untilDate = "";
      }
    }

    this.setState({
      recurrenceErrors: newRecurrenceErrors
    });
    return valid;
  };

  _proto2.componentWillUnmount = function componentWillUnmount() {
    axiosInstance.interceptors.response.eject(this.responseInterceptor);
  };

  _proto2.toggleEvent = function toggleEvent(event, e) {
    console.log("toggleEvent: " + e.eventId);
    var copySelectedEvents = [].concat(this.state.selectedEvents);

    if (event.target.checked) {
      console.log("startTime: " + e.startTime);
      copySelectedEvents.push(e);
    } else {
      var index = copySelectedEvents.indexOf(e);

      if (index !== -1) {
        copySelectedEvents.splice(index, 1);
      }
    }

    this.setState({
      selectedEvents: copySelectedEvents.sort(function (a, b) {
        return a.startTime.localeCompare(b.startTime);
      })
    });
  };

  _proto2.render = function render() {
    var _this3 = this;

    var errors = this.state.errors.map(function (e, index) {
      return /*#__PURE__*/React.createElement("div", {
        key: index,
        className: "alert alert-danger alert-dismissible",
        role: "alert"
      }, e, /*#__PURE__*/React.createElement("button", {
        type: "button",
        className: "close",
        "data-dismiss": "alert",
        "aria-label": "Close"
      }, /*#__PURE__*/React.createElement("span", {
        "aria-hidden": "true"
      }, "\xD7")));
    });
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      ref: this.myRef
    }, errors), /*#__PURE__*/React.createElement("form", null, this.state.termBooking == "enabled" && /*#__PURE__*/React.createElement(TermBookingView, {
      events: this.state.events
    }), this.state.termBooking != "enabled" && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/React.createElement("label", {
      className: "col-sm-2 col-form-label"
    }, /*#__PURE__*/React.createElement("b", null, "Available Times:")), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-10"
    }, this.state.bookingTypeBlock != "enabled" && this.state.events.map(function (e, i) {
      return /*#__PURE__*/React.createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/React.createElement("input", {
        className: "form-check-input",
        id: e.eventId + "-radio",
        value: e.eventId,
        type: "radio",
        onClick: function onClick(event) {
          return _this3.setState({
            selectedEvents: [e]
          });
        },
        defaultChecked: _this3.state.selectedEvents.map(function (e) {
          return e.eventId;
        }).includes(e.eventId),
        checked: _this3.state.selectedEvents.map(function (e) {
          return e.eventId;
        }).includes(e.eventId)
      }), /*#__PURE__*/React.createElement("label", {
        htmlFor: e.eventId + "-radio"
      }, moment(new Date(e.startTime)).format(startDateFormat), "-", moment(new Date(e.endTime)).format(endDateFormat)));
    }), this.state.bookingTypeBlock == "enabled" && this.state.events.map(function (e) {
      return /*#__PURE__*/React.createElement("div", {
        className: "form-check"
      }, /*#__PURE__*/React.createElement("input", {
        className: "form-check-input",
        id: e.eventId + "-radio",
        value: e.eventId + "-radio",
        type: "checkbox",
        onChange: function onChange(event) {
          return _this3.toggleEvent(event, e);
        },
        defaultChecked: _this3.state.selectedEvents.map(function (e) {
          return e.eventId;
        }).includes(e.eventId)
      }), /*#__PURE__*/React.createElement("label", {
        htmlFor: e.eventId + "-radio"
      }, moment(new Date(e.startTime)).format(startDateFormat), "-", moment(new Date(e.endTime)).format(endDateFormat)));
    }))), this.state.bookingTypeRecurring == "enabled" && /*#__PURE__*/React.createElement(RecurrenceInfo, {
      recurrence: this.state.recurrence,
      setRecurrence: this.setRecurrence,
      recurrenceErrors: this.state.recurrenceErrors
    }), this.state.selectedEvents && this.state.selectedEvents.length > 0 && /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-sm-2"
    }), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-10 alert alert-info"
    }, /*#__PURE__*/React.createElement("p", null, "You have selected the following event:"), /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-sm-3"
    }, /*#__PURE__*/React.createElement("b", null, "Start time:")), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-9"
    }, moment(this.state.eventMap[this.state.selectedEvents[0].eventId].startTime, moment.ISO_8601).format(dateFormat)), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-3"
    }, /*#__PURE__*/React.createElement("b", null, "End time:")), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-9"
    }, moment(this.state.eventMap[this.state.selectedEvents[this.state.selectedEvents.length - 1].eventId].endTime, moment.ISO_8601).format(dateFormat))), this.state.recurrence.enabled && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-sm-3"
    }, /*#__PURE__*/React.createElement("b", null, "Repeat frequency:")), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-9"
    }, this.state.recurrence.repeatFrequency)), /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-sm-3"
    }, /*#__PURE__*/React.createElement("b", null, "Repeat interval:")), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-9"
    }, this.state.recurrence.repeatInterval)), /*#__PURE__*/React.createElement("div", {
      className: "row"
    }, /*#__PURE__*/React.createElement("div", {
      className: "col-sm-3"
    }, /*#__PURE__*/React.createElement("b", null, "No Of Occurences:")), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-9"
    }, this.state.recurrence.noOfOccurrences)))))), /*#__PURE__*/React.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/React.createElement("label", {
      className: "col-sm-2 col-form-label"
    }, /*#__PURE__*/React.createElement("b", null, "Name:")), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-10"
    }, /*#__PURE__*/React.createElement("input", {
      id: "name",
      type: "text",
      className: "form-control",
      onChange: function onChange(e) {
        return _this3.setState({
          userName: e.target.value
        }, _this3.validate);
      }
    }), /*#__PURE__*/React.createElement("span", {
      style: {
        color: "red"
      }
    }, this.state.userNameError))), /*#__PURE__*/React.createElement("div", {
      className: "form-group row"
    }, /*#__PURE__*/React.createElement("label", {
      className: "col-sm-2 col-form-label"
    }, /*#__PURE__*/React.createElement("b", null, {
      'email': "Email",
      'phone': "Mobile",
      'email_or_phone': "Contact"
    }[this.state.bookingUserIdentification], ":")), /*#__PURE__*/React.createElement("div", {
      className: "col-sm-10"
    }, (this.state.bookingUserIdentification == "email" || this.state.bookingUserIdentification == "email_or_phone") && /*#__PURE__*/React.createElement("div", null, this.state.bookingUserIdentification == "email_or_phone" && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("input", {
      type: "radio",
      name: "channel",
      value: "email",
      defaultChecked: true,
      onClick: function onClick(e) {
        return _this3.setState({
          channel: "email",
          userMobileError: ""
        }, _this3.validate);
      }
    }), "\xA0", /*#__PURE__*/React.createElement("label", null, "Email")), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("input", {
      id: "name",
      type: "text",
      className: "form-control",
      onChange: function onChange(e) {
        return _this3.setState({
          userEmail: e.target.value
        }, _this3.validate);
      },
      disabled: this.state.channel == "mobile"
    }), /*#__PURE__*/React.createElement("span", {
      style: {
        color: "red"
      }
    }, this.state.userEmailError))), this.state.sms == "enabled" && (this.state.bookingUserIdentification == "phone" || this.state.bookingUserIdentification == "email_or_phone") && /*#__PURE__*/React.createElement("div", null, this.state.bookingUserIdentification == "email_or_phone" && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("input", {
      type: "radio",
      name: "channel",
      value: "mobile",
      onClick: function onClick(e) {
        return _this3.setState({
          channel: "mobile",
          userEmailError: ""
        }, _this3.validate);
      }
    }), "\xA0", /*#__PURE__*/React.createElement("label", null, "Mobile")), /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("input", {
      id: "name",
      type: "text",
      className: "form-control",
      onChange: function onChange(e) {
        return _this3.setState({
          userMobile: e.target.value
        }, _this3.validate);
      },
      disabled: this.state.channel == "email"
    }), this.state.countryCode != "AU" && /*#__PURE__*/React.createElement("div", {
      style: {
        color: "gray"
      }
    }, "(number must be in [+][country code][number], e.g. +15417543010 for USA)"), /*#__PURE__*/React.createElement("span", {
      style: {
        color: "red"
      }
    }, this.state.userMobileError))))), /*#__PURE__*/React.createElement("div", {
      className: "mb-2",
      style: {
        textAlign: "center"
      }
    }, this.state.recaptcha == "v2_checkbox" && /*#__PURE__*/React.createElement(Recaptcha, {
      elementID: "g-recaptcha",
      sitekey: this.props.recaptchaSiteKey
    })), /*#__PURE__*/React.createElement("div", {
      style: {
        textAlign: "center"
      }
    }, /*#__PURE__*/React.createElement("button", {
      type: "button",
      className: "btn btn-primary",
      onClick: this.makeBooking
    }, "Submit"))));
  };

  return BookingComponent;
}(React.Component);

export { BookingComponent };
//# sourceMappingURL=bnp.modern.js.map
